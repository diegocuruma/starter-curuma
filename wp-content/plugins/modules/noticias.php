<?php


/*
Plugin Name: Modulo Notícias Dev
Plugin URI: http://www.dev-undergrounnd.com.br
Description: Modulo Notícias Dev
Version: 1.0
Author: DEV
Author URI: http://www.dev-undergrounnd.com.br
*/


/* serviços */
function noticias() {
	$labels = array(
		'name'                => 'Notícias',
		'singular_name'       => 'Notícias',
		'menu_name'           => 'Notícias',
		'parent_item_colon'   => 'Notícias:',
		'all_items'           => 'Todos as Notícias',
		'view_item'           => 'Ver Notícias',
		'add_new_item'        => 'Adicionar nova notícia',
		'add_new'             => 'Nova Notícia',
		'edit_item'           => 'Editar Notícia',
		'update_item'         => 'Atualizar Notícia',
		'search_items'        => 'Notícias',
		'not_found'           => 'Nenhuma notícia Encontrado',
		'not_found_in_trash'  => 'Nenhuma notícia Encontrado na Lixeira',
	);
	$rewrite = array(
		'slug'                => 'noticia',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => false,
	);
	$args = array(
		'label'               => 'Notícias',
		'description'         => 'Notícias',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		//puxar o posttype para o menu e criar a sub sessão
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-welcome-write-blog',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'query_var'           => 'noticias',
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);
	register_post_type( 'noticias', $args );
}

add_action( 'init', 'noticias', 0 );
