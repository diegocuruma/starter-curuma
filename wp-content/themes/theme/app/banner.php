<?php
function front_banner(){ ?>                    

        <section class="painel cem cf">                
            <ul id="owl-demo" class="owl-carousel">

                <?php $painel = New wp_query ( array ( 'post_type' => 'banner','orderby'=>'date', 'order'=>'asc', 'showposts' => -1) );
                    while ( $painel -> have_posts() ): $painel -> the_post();
                    //habilitando link pelo camp personalizado
                    $habilitar = get_field("habilitar_link");
                ?>
                <li class="item">
                    <!--habilitando link pelo camp personalizado-->
                    <?php if( get_field('habilitar_link') !=False && get_field('url_do_link') !=False) { ?>        
                     <a href="<?php the_field('url_do_link'); ?>" title="<?php the_title();?>" target="_blank">
                        <?php the_post_thumbnail();?>                          
                     </a>             
                     <?php  }else{ the_post_thumbnail(); }?> 
                </li>                
               <?php endwhile; ?>
            </ul>

        </section>


<!--    <div class="pagnav"></div>	-->

<?php }
// Shortcode Banner
add_shortcode('banner', 'front_banner');

