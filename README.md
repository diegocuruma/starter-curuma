# Starter curuWork

## O projeto

O projeto DEV Underground(FW) criado por Jonas Sousa, Allef bruno e Diego Curumim deu o pontapé inicial, agora realizamos mudanças especificas para cada jeito de se trabalhar. O Starter CuruWork é um conjunto de trabalho que utiliza para as tarefas do dia a dia o grid do BT, SASS, Wordpress e Grunt.

O **CuruWork** é uma iniciativa de melhora do desenvolvimento das habilidades existentes e busca por aprendizado constante que essa profissão exige.
Dessa forma o framework vem como primeiro passo na produção conjuta de uma solução para o trabalho diário.


## O que é o framework?

É um starter baseado no trabalho DEV Underground(FW), experiências e da admiração pelo lema **DRY**.
Criamos um modo inicial de trabalho mais especifico a agência, como um boilerplate e unimos as ferramentas de SASS e GRUNTJS e o sistema Wordpress.
Esse é o primeiro passo, a contribuição está aberta e as melhorias são sempre bem vindas.


## Como usa?

O starter vem com vários exemplos de posttype's pré-carregados na HOME [shortcode], por exemplo: Logos, Painel, Serviços e etc. O arquivo que o shortcode carrega está na pasta do tema dentro de APP, não esqueça de verificar no arquivo functions. Se voce quiser usar um carrousel de logos, basta voce ir na PASTA plugins->MODULES e verificar o posttype, após isso vá no PAINEL do WP e o ative em Plugins.

Agora é só cadastrar os arquivos no painel e personalizar o respectivo arquivo dentro de APP na pasta do tema. A aplicação de novos posttypes será do mesmo jeito.

O starter vem com vários exemplos ACF's pré-definidos na pasta Originais, para facilitar um trabalho que já foi realizado. Dentro da ACF escolhida vai está um tutorial de como usa-lá.

* Versão 0.1 - Modo inicial, testes e aplicação estão começando com Bootstrap.


## Autores
**Curumim** Starter CuruWork

**Diego Curumim** Dev-Underground FW
**Allef Bruno** Dev-Underground FW
**Jonas Sousa**  Dev-Underground FW
**Isabela**  Dev-Underground FW

* Site: Dev Underground
* E-mail: diegocuruma@gmail.com

## Equipe

* Layout:
* Front:
* Back:


## Referências

* [Sass](http://sass-lang.com/)
* [Compass](http://compass-style.org/)
* [Grunt](http://gruntjs.com/)
* [Normalize CSS](http://necolas.github.io/normalize.css/)
* [HTML5 Boilerplate](http://html5boilerplate.com/)
* [HTML5 Shiv](https://github.com/aFarkas/html5shiv)
* [Clear Fix](http://nicolasgallagher.com/micro-clearfix-hack/)
